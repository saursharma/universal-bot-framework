"use strict";

var fs = require("fs");
var path = require("path");
var mongoose = require("mongoose");

function __setupMongoDatabase() {
    var dbURI = global.bots.config.mongoDb;
    // Use native promises
    mongoose.Promise = global.Promise;

    mongoose.connection.on('connected', function () {
        console.log('database:mongo connection established successfully...');
    });
    mongoose.connection.on('error', function (error) {
        console.error('database:mongo connection error: ', error);
    });
    mongoose.connection.on('disconnected', function () {
        console.error('database:mongo connection disconnected');
    });
    console.log("connecting to database:mongo...");
    var dbOptions = {
        'replset': {
            'readPreference': 'nearest',
            'strategy': 'ping',
            'rs_name': 'rs0',
            'socketOptions': {
                'keepAlive': 1,
                'connectTimeoutMS': 30000
            }
        },
        server: {
            poolSize: 2,
            socketOptions: {
                keepAlive: 1,
                connectTimeoutMS: 30000
            }
        }
    };
    mongoose.connect(dbURI, dbOptions);
}
__setupMongoDatabase();

var mongo = {};

fs
    .readdirSync(__dirname)
    .filter(function (file) {
        return (file.indexOf(".") !== 0) && (file !== "index.js") && (file !== "old");
    })
    .forEach(function (file) {
        var model = require(path.join(__dirname, file));
        mongo[model.modelName] = model;
    });

/**
 * checks if the connection is established
 * @param callback function(isConnected)
 */
mongo.isConnected = function isConnected(callback) {
    // Connection ready state
    var state = {
        0: "disconnected",
        1: "connected",
        2: "connecting",
        3: "disconnecting",
        4: "disconnected"
    };
    if (mongoose.connection.readyState === 1)
        return callback(true); // success

    console.error("Mongo:isConnected failed: readyState:", mongoose.connection.readyState, state[mongoose.connection.readyState]);
    callback(false); // failed
};

module.exports = mongo;