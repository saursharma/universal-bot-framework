// BASIC FRAMEWORKS
// ---------------------
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// MODEL SCHEMA
// ---------------------
var ChatBotUserSessionSchema = new Schema({
    id: {
        type: mongoose.Schema.Types.ObjectId,
        index: true
    },
    chatBotPlatform: String,
    chatBotUserId: String,
    profile: {},
    chatHistory: {type: mongoose.Schema.Types.Array, default: []},
    settings: {
        lastMessage: {},
        location: {
            lat: {type: Number, default: null},
            long: {type: Number, default: null}
        },
        specialAction: String
    },
    lastReq: {type: mongoose.Schema.Types.Mixed},
    createdAt: {type: Date, default: Date.now, index: true}
});

// =============================================================================
// INDEX
// =============================================================================

// =============================================================================
// METHODS
// =============================================================================

ChatBotUserSessionSchema.pre('save', function (next) {
    // remove null values, to not saving to mongo

    if (this.lastReq) {
        // just keep the headers
        var headers = this.lastReq.headers;
        var body = this.lastReq.body;
        this.lastReq = undefined;
        this.lastReq = {
            headers: headers,
            body: body
        };
    }

    next();
});

// =============================================================================
// EXPORTS
// =============================================================================
var ChatBotUserSession = mongoose.model('ChatBotUserSession', ChatBotUserSessionSchema);
module.exports = ChatBotUserSession;