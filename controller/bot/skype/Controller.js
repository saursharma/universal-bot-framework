// BASIC FRAMEWORKS
// ---------------------

// MODELS
// ---------------------
var model = require('../../../model');

// CONTROLLERS
// ---------------------
var util = require("../../../util");
var controller = require("../../../controller");

// CONST
// ---------------------
var constant = require("../../../constant");
var _chatBotPlatform = "Skype"; // case-sensitive, must be the same as in config.bot.json and bot/index.js
var _bot = require("./Listener"),
    _maxButtons = 3,
    _maxElements = 30;

/**
 * sends a welcome message to the user
 * @param chatBotUserSession
 */
exports.sendWelcomeMessage = function sendWelcomeMessage(chatBotUserSession) {
    //var text = constant.BOT.MESSAGE.WELCOME.replace(":first_name:", chatBotUserSession.profile.first_name);
    //exports.sendText(chatBotUserSession, text);
    //exports.sendCommandChooser(chatBotUserSession);
};

/**
 * send command chooser
 * @param chatBotUserSession
 */
exports.sendCommandChooser = function sendCommandChooser(chatBotUserSession) {
    //var text = constant.BOT.MESSAGE.WHAT_WOULD_YOU_LIKE_TO_DO;
    //
    //var markup = _bot.keyboard([
    //    [constant.BOT.MESSAGE.START_VOTING],
    //    [constant.BOT.MESSAGE.CHOOSE_A_CATEGORY],
    //    [constant.BOT.MESSAGE.SEND_LINK]
    //], {resize: true});
    //
    //_bot.sendMessage(chatBotUserSession.chatBotUserId, text, {markup});
};

/**
 * send category chooser
 * @param chatBotUserSession
 * @param categories
 */
exports.sendCategoryChooserWithScrollView = function sendCategoryChooserWithScrollView(chatBotUserSession, categories) {
    //var text = constant.BOT.MESSAGE.SELECT_A_CATEGORY,
    //    elements = [],
    //    counter = 0,
    //    emoji;
    //
    //// back to menu
    //elements.push([constant.BOT.MESSAGE.BACK_TO_MENU]);
    //
    //var rowElements = [];
    //
    //for (var i = 0; i < categories.length; i++) {
    //    rowElements = [];
    //    while (rowElements.length < 2 && i < categories.length) {
    //        if (categories[i] && (categories[i].slug == "all" || categories[i].show.exploring && categories[i].show.creating) &&
    //            counter < _maxElements
    //        ) {
    //            rowElements.push(categories[i].name);
    //            counter++;
    //        }
    //        if (rowElements.length < 2) i++;
    //    }
    //    if (rowElements.length == 1) {
    //        rowElements.push(" ");
    //    }
    //    elements.push(rowElements);
    //}
    //
    //var markup = _bot.keyboard(elements, {resize: true});
    //_bot.sendMessage(chatBotUserSession.chatBotUserId, text, {markup})
    //    .catch(function (error) {
    //        console.error("sendCategoryChooserWithScrollView:sendMessage", error);
    //    });
};

/**
 * send dvel to vote
 * @param chatBotUserSession
 * @param dvel
 */
exports.sendDvelToVote = function sendDvelToVote(chatBotUserSession, dvel) {

    //_bot.sendPhoto(chatBotUserSession.chatBotUserId, constant.BOT.MESSAGE.SWELL_SHARING_IMAGE_URL_PREFIX + (global.bots.config.production ? dvel.shortId : "ryXP28Gz"),
    //    {
    //        caption: dvel.user.name + ": " + (dvel.question || "#whatsBetter"),
    //        fileName: (global.bots.config.production ? dvel.shortId : "ryXP28Gz") + ".png",
    //        reply_markup: _bot.inlineKeyboard([
    //            [
    //                _bot.inlineButton(constant.BOT.MESSAGE.VOTE_A_EMOJI, {
    //                    callback: JSON.stringify({action: constant.BOT.MESSAGE.VOTE_A})
    //                    // cant use payload here, because max size = 64 bytes
    //                    //JSON.stringify({
    //                    //    action: constant.BOT.MESSAGE.DO_VOTE_ACTION,
    //                    //    dvelId: dvel.id,
    //                    //    photoId: dvel.photos[0].id
    //                    //})
    //                }),
    //                _bot.inlineButton(constant.BOT.MESSAGE.SKIP_EMOJI, {
    //                    callback: JSON.stringify({action: constant.BOT.MESSAGE.SKIP})
    //                    // cant use payload here, because max size = 64 bytes
    //                    //JSON.stringify({
    //                    //action: constant.BOT.MESSAGE.DO_SKIP_ACTION,
    //                    //dvelId: dvel.id
    //                    //})
    //                }),
    //                _bot.inlineButton(constant.BOT.MESSAGE.VOTE_B_EMOJI, {
    //                    callback: JSON.stringify({action: constant.BOT.MESSAGE.VOTE_B})
    //                    // cant use payload here, because max size = 64 bytes
    //                    //JSON.stringify({
    //                    //    action: constant.BOT.MESSAGE.DO_VOTE_ACTION,
    //                    //    dvelId: dvel.id,
    //                    //    photoId: dvel.photos[1].id
    //                    //})
    //                })
    //            ], [
    //                _bot.inlineButton(constant.BOT.MESSAGE.OPEN_IN_APP_EMOJI, {
    //                    url: "http://swell.wtf/d/"+dvel.shortId
    //                })
    //            ]
    //        ])
    //    })
    //    .catch(function (error) {
    //        console.error("sendDvelToVote:sendPhoto", error);
    //    });
};

/**
 * this method gets called from Bot.Controller
 * it returns a generic user profile with the same keys for every chat bot platform
 * @param chatBotUserId
 * @param callback function(profile), profile could be null if not found
 */
exports.getUserProfileGeneric = function getUserProfileGeneric(chatBotUserId, callback) {
    //console.log("getUserProfileGeneric", chatBotUserId);
    //_bot.getChat(chatBotUserId).then(function (result) {
    //    console.log("getUserProfileGeneric:getChat", result);
    //    if (result.result) {
    //        __getProfilePic(chatBotUserId, function (error, profilePicUrl) {
    //            callback({
    //                id: chatBotUserId,
    //                first_name: result.result.first_name,
    //                last_name: result.result.last_name,
    //                profile_pic: profilePicUrl,
    //                locale: null,
    //                timezone: null,
    //                gender: result.result.gender ? result.result.gender == "male" ? 0 : 1 : 99
    //            });
    //        });
    //    } else {
    //        callback(null);
    //    }
    //}).catch(function (error) {
    //    console.error("getUserProfileGeneric:getChat", error);
    //    callback(null);
    //});
};

/**
 * fetch the url of the first profile picture (highest solution)
 * @param chatBotUserId
 * @param callback function(error, profilePicUrl)
 * @private
 */
function __getProfilePic(chatBotUserId, callback) {
    //_bot.getUserPhoto(chatBotUserId).then(function (result) {
    //    if (result.result && result.result.total_count > 0) {
    //        var photos = result.result.photos[0], // first profile pic we found
    //            maxSize = 0,
    //            fileId;
    //        photos.forEach(function (photo) {
    //            if (photo.file_size > maxSize) {
    //                maxSize = photo.file_size;
    //                fileId = photo.file_id;
    //            }
    //        });
    //        _bot.getFile(fileId).then(function (result) {
    //            //console.log("getFile", result);
    //            //console.log("url:", bot.fileLink + result.result.file_path); // https://api.telegram.org/file/bot<token>/<file_path>
    //            callback(null, _bot.fileLink + result.result.file_path);
    //        }).catch(function (error) {
    //            console.error("getFile", error);
    //            callback(error, null);
    //        })
    //    } else {
    //        callback("no profile pics", null);
    //    }
    //}).catch(function (error) {
    //    console.error("getUserPhoto", error);
    //    callback(error, null);
    //});
}

/**
 * send text
 * @param chatBotUserSession
 * @param text
 */
exports.sendText = function sendText(chatBotUserSession, text) {
    _bot.send(chatBotUserSession.chatBotUserId, true, function (error, result) {
        console.log(error, result);
    });
};

/**
 * send link
 * @param chatBotUserSession
 * @param link
 * @param text
 * @param picUrl
 */
exports.sendLink = function sendLink(chatBotUserSession, link, text, picUrl) {
    //_bot.sendPhoto(chatBotUserSession.chatBotUserId, picUrl,
    //    {
    //        caption: "",
    //        //fileName: "ryXP28Gz.png",
    //        reply_markup: _bot.inlineKeyboard([
    //            [
    //                _bot.inlineButton(text, {url: link})
    //            ]
    //        ])
    //    })
    //    .catch(function (error) {
    //        console.error("sendPhoto", error);
    //    });
};

/**
 * remove voting buttons
 * @param chatBotUserSession
 */
exports.removeButtons = function removeButtons(chatBotUserSession) {

};

/**
 * sends trending tags to user
 * @param chatBotUserSession
 */
exports.sendTrendingTags = function sendTrendingTags(chatBotUserSession) {
    //var trendingTags = controller.Bot.Handler.getTrendingTags(_maxQuickReplies);
    //var quickReplies = [];
    //trendingTags.forEach(function (tag) {
    //    quickReplies.push({
    //        "content_type": "text",
    //        "title": tag,
    //        "payload": JSON.stringify({action: tag})
    //    });
    //});

    // ...
};