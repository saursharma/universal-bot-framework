// BASIC FRAMEWORKS
// ---------------------
var Bot = require('@kikinteractive/kik');

// MODELS
// ---------------------
var model = require('../../../model');

// CONTROLLERS
// ---------------------
var util = require("../../../util");
var controller = require("../../../controller");

// CONST
// ---------------------
var constant = require("../../../constant");
var _chatBotPlatform = "Kik"; // case-sensitive, must be the same as in config.bot.json and bot/index.js
var _bot = require("./Listener"),
    _maxElements = 20;

/**
 * sends a welcome message to the user
 * @param chatBotUserSession
 */
exports.sendWelcomeMessage = function sendWelcomeMessage(chatBotUserSession) {
    var text = constant.BOT.MESSAGE.WELCOME.replace(":first_name:", chatBotUserSession.profile.first_name);
    _bot.send(Bot.Message.text(text), chatBotUserSession.chatBotUserId);
    exports.sendCommandChooser(chatBotUserSession);
};

/**
 * send command chooser
 * @param chatBotUserSession
 */
exports.sendCommandChooser = function sendCommandChooser(chatBotUserSession) {
    _bot.send(Bot.Message.text(constant.BOT.MESSAGE.WHAT_WOULD_YOU_LIKE_TO_DO)
        .addTextResponse(constant.BOT.MESSAGE.START_VOTING)
        .addTextResponse(constant.BOT.MESSAGE.CHOOSE_A_CATEGORY)
        .addTextResponse(constant.BOT.MESSAGE.TRENDING_TAGS)
        .addTextResponse(constant.BOT.MESSAGE.SEND_LINK),
        chatBotUserSession.chatBotUserId);
};

/**
 * send category chooser
 * @param chatBotUserSession
 * @param categories
 */
exports.sendCategoryChooserWithScrollView = function sendCategoryChooserWithScrollView(chatBotUserSession, categories) {
    var message = Bot.Message.text(constant.BOT.MESSAGE.SELECT_A_CATEGORY),
        counter = 0;

    // back to menu
    message.addTextResponse(constant.BOT.MESSAGE.BACK_TO_MENU);

    for (var i in categories) {
        if (categories.hasOwnProperty(i) && counter < _maxElements
        ) {
            message.addTextResponse(categories[i].name);
        }
        counter++;
    }
    _bot.send(message, chatBotUserSession.chatBotUserId);
};

/**
 * send dvel to vote
 * @param chatBotUserSession
 * @param dvel
 */
exports.sendDvelToVote = function sendDvelToVote(chatBotUserSession, dvel) {

    _bot.send(
        [
            Bot.Message.text(dvel.user.name + ": " + (dvel.question || "#whatsBetter")),
            Bot.Message.picture(constant.BOT.MESSAGE.SWELL_SHARING_IMAGE_URL_PREFIX + (global.bots.config.production ? dvel.shortId : "ryXP28Gz"))
                .setAttributionName(" ")
                .setAttributionIcon(constant.BOT.MESSAGE.FAVICON_URL)
                .addTextResponse(constant.BOT.MESSAGE.VOTE_A_EMOJI)
                .addTextResponse(constant.BOT.MESSAGE.VOTE_B_EMOJI)
                .addTextResponse(constant.BOT.MESSAGE.OPEN_IN_APP)
                .addTextResponse(constant.BOT.MESSAGE.SKIP_EMOJI)
                .addTextResponse(constant.BOT.MESSAGE.CANCEL)
        ],
        chatBotUserSession.chatBotUserId);
};

/**
 * send text
 * @param chatBotUserSession
 * @param text
 */
exports.sendText = function sendText(chatBotUserSession, text) {
    _bot.send(Bot.Message.text(text), chatBotUserSession.chatBotUserId);
};

/**
 * send link
 * @param chatBotUserSession
 * @param link
 * @param text
 * @param picUrl
 */
exports.sendLink = function sendLink(chatBotUserSession, link, text, picUrl) {
    var message = Bot.Message.link(link);
    if (text) message.setText(text);
    //if (text) message.setTitle(text);
    if (picUrl) message.setPicUrl(picUrl);
    message.setAttributionIcon(constant.BOT.MESSAGE.FAVICON_URL);
    message.setAttributionName(link.replace("http://www.", ""));
    message.addTextResponse(constant.BOT.MESSAGE.START_VOTING);
    message.addTextResponse(constant.BOT.MESSAGE.CHOOSE_A_CATEGORY);
    message.addTextResponse(constant.BOT.MESSAGE.SEND_LINK);
    _bot.send(message, chatBotUserSession.chatBotUserId);
};

/**
 * this method gets called from Bot.Controller
 * it returns a generic user profile with the same keys for every chat bot platform
 * @param chatBotUserId
 * @param callback function(profile), profile could be null if not found
 */
exports.getUserProfileGeneric = function getUserProfileGeneric(chatBotUserId, callback) {
    console.log("getUserProfileGeneric", chatBotUserId);
    _bot.getUserProfile(chatBotUserId)
        .then(function (user) {
            //console.log("getUserProfileGeneric:getUserProfile", user);
            if (user)
                callback({
                    id: chatBotUserId,
                    first_name: user.firstName,
                    last_name: user.lastName,
                    profile_pic: user.profilePicUrl,
                    locale: null,
                    timezone: null,
                    gender: user.gender ? user.gender == "male" ? 0 : 1 : 99
                });
            else {
                callback(null);
            }
        });
};

/**
 * remove voting buttons
 * @param chatBotUserSession
 */
exports.removeButtons = function removeButtons(chatBotUserSession) {
    if (chatBotUserSession.settings.lastMessage && chatBotUserSession.settings.lastMessage.chatId && chatBotUserSession.settings.lastMessage.messageId) {

        chatBotUserSession.settings.lastMessage = null;
        chatBotUserSession.save(function (error, result) {
            if (error) console.error("removeButtons.settings.lastMessage save", error);
        });
    }
};

/**
 * ask for users location
 * @param chatBotUserSession
 */
exports.askForLocation = function askForLocation(chatBotUserSession) {
    chatBotUserSession.settings.specialAction = constant.BOT.SPECIAL_ACTION.ASK_FOR_LOCATION;
    chatBotUserSession.save(function (error, result) {
        if (error) console.error("askForLocation:chatBotUserSession.settings.specialAction save", error);
    });

    var text = constant.BOT.MESSAGE.ASK_FOR_LOCATION;

    _bot.send(Bot.Message.text(text)
        //.addTextResponse(constant.BOT.MESSAGE.NO_LATER)
        ,
        chatBotUserSession.chatBotUserId);

    // TODO maybe send a link to a webpage which then calls the endpoint and the endpoint does a IP to location and adds it to the userSession
};

/**
 * gets called if location not found through address search
 * @param chatBotUserSession
 */
exports.locationNotFoundAskAgain = function locationNotFoundAskAgain(chatBotUserSession) {
    var text = constant.BOT.MESSAGE.SRY_CANT_FIND_CITY;

    _bot.send(Bot.Message.text(text)
        //.addTextResponse(constant.BOT.MESSAGE.CANCEL)
        ,
        chatBotUserSession.chatBotUserId);
};

/**
 * sends trending tags to user
 * @param chatBotUserSession
 */
exports.sendTrendingTags = function sendTrendingTags(chatBotUserSession) {
    var trendingTags = controller.Bot.Handler.getTrendingTags(_maxElements);
    var message = Bot.Message.text(constant.BOT.MESSAGE.TRENDING_TAGS + ":"),
        counter = 0;

    // back to menu
    message.addTextResponse(constant.BOT.MESSAGE.BACK_TO_MENU);

    trendingTags.forEach(function (tag) {
        if (counter < _maxElements - 1) { // -1 because of menu
            message.addTextResponse(tag);
        }
        counter++;
    });

    _bot.send(message, chatBotUserSession.chatBotUserId, function (error) {
        if (error) {
            console.error(error);
        }
    });
};